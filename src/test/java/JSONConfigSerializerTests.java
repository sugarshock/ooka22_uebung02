import Configs.ConfigEntry;
import Enums.STATUS;
import Configs.Serializer.ConfigSerializer;
import Configs.Serializer.JSONConfigSerializer;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class JSONConfigSerializerTests {
    private static ConfigSerializer configSerializer= new JSONConfigSerializer();
    private static String EXPORT_PATH = "src\\test\\resources\\config.json";

    @Test
    public void testSerializeDeserializeRunning() throws IOException {
        UUID uuid = UUID.randomUUID();
        String fakeCompPath = "\\this\\is\\a\\path.jar";

        List<ConfigEntry> config = new ArrayList<>();
        ConfigEntry entry = new ConfigEntry(uuid, fakeCompPath, STATUS.RUNNING);
        config.add(entry);

        configSerializer.toFile(config, EXPORT_PATH);
        List<ConfigEntry> impConfig = configSerializer.fromFile(EXPORT_PATH);

        assertEquals(config.size(), impConfig.size());
        assertEquals(entry.uuid, impConfig.get(0).uuid);
        assertEquals(entry.path, impConfig.get(0).path);
        assertEquals(entry.status, impConfig.get(0).status);
    }

    @Test
    public void testSerializeDeserializeStopped() throws IOException {
        UUID uuid = UUID.randomUUID();
        String fakeCompPath = "\\this\\is\\a\\path.jar";

        List<ConfigEntry> config = new ArrayList<>();
        ConfigEntry entry = new ConfigEntry(uuid, fakeCompPath, STATUS.STOPPED);
        config.add(entry);

        configSerializer.toFile(config, EXPORT_PATH);
        List<ConfigEntry> impConfig = configSerializer.fromFile(EXPORT_PATH);

        assertEquals(config.size(), impConfig.size());
        assertEquals(entry.uuid, impConfig.get(0).uuid);
        assertEquals(entry.path, impConfig.get(0).path);
        assertEquals(entry.status, impConfig.get(0).status);
    }

}
