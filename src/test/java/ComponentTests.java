import Component.Component;
import Enums.STATUS;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import java.io.File;
import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class ComponentTests {

    private static String COMP_PATH = "src/test/resources/hotelsucheComponent.jar";

    @BeforeAll
    public static void testIfComponentExists(){
        File file = new File(COMP_PATH);
        assertTrue(file.exists());
    }

    @Test
    public void testCreatingSingleComponent(){
        File file = new File(COMP_PATH);
        UUID id = UUID.randomUUID();
        Component component = null;
        try {
            component = new Component(id, COMP_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        assertNotNull(component);
        assertEquals(id, component.getId());
        assertEquals(STATUS.STOPPED, component.getStatus());
    }

    @Test
    public void testCreatingTwoInstancesOfSameComponent(){
        Component c1 = null;
        Component c2 = null;
        try {
            c1 = new Component(UUID.randomUUID(), COMP_PATH);
            c2 = new Component(UUID.randomUUID(), COMP_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        assertNotEquals(c1.getStartClassForTestingPurposes(), c2.getStartClassForTestingPurposes());
    }
}
