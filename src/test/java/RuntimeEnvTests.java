import Component.Component;
import Enums.STATUS;
import RuntimeEnv.RuntimeEnv;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class RuntimeEnvTests {
        private static String COMP_PATH = "src\\test\\resources\\hotelsucheComponent.jar";


        private static RuntimeEnv runtimeEnv;
        private static UUID componentId;

        @BeforeAll
        public static void createRuntimeWithSingleComponent() throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
                runtimeEnv = new RuntimeEnv();
                assertEquals(STATUS.STOPPED, runtimeEnv.getStatus());
                componentId = runtimeEnv.loadComponent(COMP_PATH, false);
        }

        @Test
        public void testLoadingComponent() {
                Component component = runtimeEnv.getComponents().get(componentId);
                assertNotNull(component);
                assertEquals(componentId, component.getId());
                assertEquals(STATUS.STOPPED, component.getStatus());
        }

        @Test
        public void testStartingEnv(){
                Component component = runtimeEnv.getComponents().get(componentId);
                try {
                        runtimeEnv.start();
                } catch (InvocationTargetException e) {
                        e.printStackTrace();
                }
                assertEquals(STATUS.RUNNING,runtimeEnv.getStatus());
                assertEquals(STATUS.RUNNING,component.getStatus());
        }

        @Test
        public void testForking() throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException {
                Component c1 = runtimeEnv.getComponents().get(componentId);
                UUID id2 = runtimeEnv.forkComponent(c1.getId());
                Component c2 = runtimeEnv.getComponents().get(id2);

                assertNotNull(c2);
                assertEquals(STATUS.RUNNING,c1.getStatus());
                assertEquals(STATUS.STOPPED,c2.getStatus());
                assertNotEquals(c1.getStartClassForTestingPurposes(), c2.getStartClassForTestingPurposes());
        }

        @Test
        public void testStoppingEnv() throws InvocationTargetException {
                Component component = runtimeEnv.getComponents().get(componentId);
                runtimeEnv.stop();
                assertEquals(STATUS.STOPPED,runtimeEnv.getStatus());
                assertEquals(STATUS.STOPPED,component.getStatus());
        }

        @Test
        public void testExportImportConfig() throws InvocationTargetException {
                var config = runtimeEnv.getCurrentConfig();
                runtimeEnv.recoverFromConfig(config);
                assertEquals(1, runtimeEnv.getComponents().size());
                assertNotNull(runtimeEnv.getComponents().get(componentId));
                assertEquals(COMP_PATH, runtimeEnv.getComponents().get(componentId).getComponentPath());
        }


}
