package Configs;

import Enums.STATUS;

import java.io.Serializable;
import java.util.UUID;


public class ConfigEntry implements Serializable {
    public UUID uuid;
    public String path;
    public STATUS status;

    public ConfigEntry(){

    }

    public ConfigEntry(UUID uuid, String path, STATUS status){
        this.uuid = uuid;
        this.path = path;
        this.status = status;
    }
}
