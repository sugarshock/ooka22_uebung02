package Configs.Serializer;

import Configs.ConfigEntry;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JSONConfigSerializer implements  ConfigSerializer{
    @Override
    public File toFile(List<ConfigEntry> config, String path) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File(path);
        objectMapper.writeValue(file, config);
        return file;
    }

    @Override
    public List<ConfigEntry> fromFile(String path) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File(path);
        TypeReference<List<ConfigEntry>> typeRef = new TypeReference<List<ConfigEntry>>(){};
        List<ConfigEntry> config = objectMapper.readValue(file, typeRef);
        return config;
    }
}
