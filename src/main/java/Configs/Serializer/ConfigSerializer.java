package Configs.Serializer;

import Configs.ConfigEntry;
import Enums.STATUS;
import org.javatuples.Triplet;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface ConfigSerializer {
    public File toFile(List<ConfigEntry> config, String path) throws IOException;
    public List<ConfigEntry> fromFile(String path) throws IOException;
}
