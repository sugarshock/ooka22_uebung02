package Application;

import CLI.ComponentCmd;
import CLI.HelpCmd;
import CLI.RuntimeCmd;
import Configs.Serializer.ConfigSerializer;
import Configs.Serializer.JSONConfigSerializer;
import picocli.CommandLine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Application {
    public static boolean running;
    public static ConfigSerializer configSerializer;

    public static void main(String[] args) throws IOException {
        String prompt;
        running = true;
        configSerializer = new JSONConfigSerializer();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("This is the LZU Runtime Environment shell interface. Please call \"help\" to learn more.");

        while (true){
            System.out.println("LZU:>");
            prompt = reader.readLine();
            String[] array = prompt.split(" ");
            String[] arguments = Arrays.copyOfRange(array, 1, array.length);

            switch(array[0]){
                case "runtime":
                    CommandLine.run(new RuntimeCmd(), arguments);
                    break;
                case "component":
                    CommandLine.run(new ComponentCmd(), arguments);
                    break;
                default:
                    CommandLine.run(new HelpCmd(), arguments);
            }

        }
    }


}
