package CLI;

import RuntimeEnv.RuntimeEnv;
import picocli.CommandLine;

import java.lang.reflect.InvocationTargetException;

@CommandLine.Command(name = "stop")
public class RuntimeStopCmd implements Runnable {

    @Override
    public void run() {
        try {
            RuntimeEnv.getInstance().stop();
        } catch (InvocationTargetException e) {
            System.out.println("Was unable to shutdown Runtime Environment properly:");
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("Stopped Runtime Environment successfully!");
    }
}
