package CLI;

import Application.Application;
import Configs.ConfigEntry;
import Enums.STATUS;
import RuntimeEnv.RuntimeEnv;
import org.javatuples.Triplet;
import picocli.CommandLine;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.UUID;

@CommandLine.Command(name = "export")
public class RuntimeExportCmd implements Runnable{

    @CommandLine.Option(names = {"-p", "--path"}, required = true)
    private String path;

    @Override
    public void run() {
        System.out.println("Preparing for config export...");
        List<ConfigEntry> config = RuntimeEnv.getInstance().getCurrentConfig();
        try {
            Application.configSerializer.toFile(config, path);
            System.out.println("Successfully exported current config to " + path + " !");
        } catch (IOException e) {
            System.out.println("Unable to access the specified file path!");
            System.out.println(e.getMessage());
        }

    }
}
