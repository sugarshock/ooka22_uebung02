package CLI;

import RuntimeEnv.RuntimeEnv;
import picocli.CommandLine;

import javax.naming.OperationNotSupportedException;
import java.util.UUID;

public class RuntimeRemoveCmd implements Runnable{

    @CommandLine.Option(names = {"-id"})
    private String id;

    @Override
    public void run() {
        try {
            RuntimeEnv.getInstance().removeComponent(UUID.fromString(id));
            System.out.println("Successfully removed component from Runtime Environment!");
        } catch (OperationNotSupportedException e) {
            System.out.println(e.getMessage());
        }catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
