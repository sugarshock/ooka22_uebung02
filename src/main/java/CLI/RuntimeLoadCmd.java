package CLI;

import RuntimeEnv.RuntimeEnv;
import picocli.CommandLine;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

@CommandLine.Command(name = "load")
public class RuntimeLoadCmd implements Runnable {

    @CommandLine.Option(names = {"-p", "--path"}, required = true)
    private String path;

    @CommandLine.Option(names = {"-s", "--start"})
    private boolean start;

    @Override
    public void run() {
        try {
            UUID id = RuntimeEnv.getInstance().loadComponent(path, start);
            System.out.println("Successfully loaded component with id " + id);
        } catch (IOException e) {
            System.out.println("Unable to load file from path!");
        } catch (ClassNotFoundException e) {
            System.out.println("Unable to find proper StartClass!");
        } catch (NoSuchMethodException e) {
            System.out.println("Unable to find necessary Start and Stop methods!");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
