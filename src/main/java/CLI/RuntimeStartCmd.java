package CLI;

import RuntimeEnv.RuntimeEnv;
import picocli.CommandLine;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "start")
public class RuntimeStartCmd implements Runnable {
    @Override
    public void run() {
        try{
            RuntimeEnv.getInstance().start();
        }catch(InvocationTargetException e){
            System.out.println("Was unable to start one or more loaded components.");
            e.printStackTrace();
        }
        System.out.println("Runtime Environment started!");
    }
}
