package CLI;

import RuntimeEnv.RuntimeEnv;
import picocli.CommandLine;

import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Callable;

@CommandLine.Command(
        subcommands = {
                RuntimeStartCmd.class,
                RuntimeStopCmd.class,
                RuntimeStatusCmd.class,
                RuntimeLoadCmd.class
        }
)
public class RuntimeCmd implements Runnable {


    @Override
    public void run() {
        System.out.println("Hmmmm...");
    }
}
