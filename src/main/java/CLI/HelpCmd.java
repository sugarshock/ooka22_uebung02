package CLI;

import picocli.CommandLine;

@CommandLine.Command
public class HelpCmd implements Runnable{

    @Override
    public void run() {
        StringBuilder builder = new StringBuilder("LZU supports the following commands:\n");
        builder.append("help\n");
        builder.append("runtime start\n");
        builder.append("runtime stop\n");
        builder.append("runtime status\n");
        builder.append("runtime load -p [path-to-jar]\n");
        builder.append("runtime remove -id [id]\n");
        builder.append("runtime export -path [path-to-json]\n");
        builder.append("runtime import -path [path-to-json]\n");
        builder.append("component start -id [id]\n");
        builder.append("component stop -id [id]\n");

        System.out.println(builder.toString());
    }
}
