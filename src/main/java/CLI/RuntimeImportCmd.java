package CLI;

import Application.Application;
import Configs.ConfigEntry;
import Enums.STATUS;
import RuntimeEnv.RuntimeEnv;
import org.javatuples.Triplet;
import picocli.CommandLine;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.UUID;

@CommandLine.Command(name = "import")
public class RuntimeImportCmd implements Runnable{

    @CommandLine.Option(names = {"-p", "--path"}, required = true)
    private String path;

    @Override
    public void run() {
        System.out.println("Importing config file...");

        List<ConfigEntry> config;
        try {
            config = Application.configSerializer.fromFile(path);
            System.out.println("Successfully exported current config to " + path + " !");
        } catch (IOException e) {
            System.out.println("Unable to access the specified file path!");
            System.out.println(e.getMessage());
            return;
        }

        System.out.println("Shutting down for import...");
        try{
            RuntimeEnv.getInstance().recoverFromConfig(config);
        } catch (InvocationTargetException e) {
            System.out.println("Unable to properly shut down runtime for import! Restarting...");
            try {
                RuntimeEnv.getInstance().start();
            } catch (InvocationTargetException ex) {
                System.out.println("Unable to reboot runtime. Illegal state.");
            }
        }
    }
}
