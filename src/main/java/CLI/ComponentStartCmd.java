package CLI;

import RuntimeEnv.RuntimeEnv;
import picocli.CommandLine;

import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

@CommandLine.Command(name = "start")
public class ComponentStartCmd implements Runnable{

    @CommandLine.Option(names = {"-id"})
    private String id;

    @Override
    public void run() {
        var comp = RuntimeEnv.getInstance().getComponents().get(UUID.fromString(id));
        if(comp == null){
            System.out.println("No Component with this ID is loaded!");
            return;
        }

        try {
            comp.start();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
