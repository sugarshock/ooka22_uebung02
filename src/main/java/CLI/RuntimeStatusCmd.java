package CLI;

import RuntimeEnv.RuntimeEnv;
import picocli.CommandLine;

@CommandLine.Command(name = "status")
public class RuntimeStatusCmd implements Runnable{
    @Override
    public void run() {
        System.out.println(RuntimeEnv.getInstance().getFullStatus());
    }
}
