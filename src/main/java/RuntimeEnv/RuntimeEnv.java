package RuntimeEnv;

import Component.Component;
import Configs.ConfigEntry;
import Enums.STATUS;
import Logging.ConsoleLogger;
import Logging.Logger;

import javax.naming.OperationNotSupportedException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class RuntimeEnv {
    private static RuntimeEnv instance;

    private Logger logger = new ConsoleLogger();
    private Map<UUID, Component> components = new LinkedHashMap();
    private STATUS status = STATUS.STOPPED;

    public String getFullStatus(){
        StringBuilder builder = new StringBuilder("Runtime Environment Status: ");
        builder.append(status.name());
        builder.append("\nLoaded Components:\n");
        components.values().forEach(c -> builder.append(c.toString() + "\n"));
        return builder.toString();
    }

    public STATUS getStatus(){
        return status;
    }

    public UUID loadComponent(String path, boolean start, UUID id) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Component component = new Component(id,path);
        components.put(component.getId(), component);
        if(start) {
            component.start();
        }
        return  component.getId();
    }

    public UUID loadComponent(String path, boolean start) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return  loadComponent(path,start,UUID.randomUUID());
    }

    public UUID forkComponent(UUID id) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException {
        Component old = components.get(id);
        if(old == null){
            throw new IllegalArgumentException("Component with ID" + id + "does not exist!");
        }
        Component newC = new Component(UUID.randomUUID(), old);
        components.put(newC.getId(), newC);
        return newC.getId();
    }

    public void removeComponent(UUID id) throws OperationNotSupportedException, IllegalArgumentException {
        Component component = components.get(id);
        if(component == null){
            throw new IllegalArgumentException("Component with ID" + id + "does not exist!");
        }
        if(component.getStatus() == STATUS.RUNNING){
            throw new OperationNotSupportedException("Component with ID" +  id + "is still running! Stop it before removing it.");
        }

        components.remove(id);
    }

    public void start() throws InvocationTargetException {
        for (Component comp : components.values()) {
            if(comp.getStatus() == STATUS.STOPPED){
                comp.start();
            }
        }

        status = STATUS.RUNNING;
    }

    public void stop() throws InvocationTargetException {
        status = STATUS.STOPPED;
        for (Component comp : components.values()) {
            if(comp.getStatus() == STATUS.RUNNING){
                try {
                    comp.stop();
                } catch (InvocationTargetException e) {
                    System.out.println("Unable to stop component " + comp);
                    status = STATUS.RUNNING;
                    throw e;
                }
            }
        }
    }

    public Map<UUID, Component> getComponents(){
        return new HashMap<>(components);
    }

    public List<ConfigEntry> getCurrentConfig(){
        List<ConfigEntry> list = new ArrayList<>();
        for(Component component : components.values()){
            ConfigEntry entry =
                    new ConfigEntry(component.getId(), component.getComponentPath(), component.getStatus());
            list.add(entry);
        }
        return list;
    }

    public void recoverFromConfig(List<ConfigEntry> config) throws InvocationTargetException {
        if(status == STATUS.RUNNING){
            stop();
        }

        components = new HashMap<>();

        for(ConfigEntry entry : config){
            UUID compId = null;
            try {
                compId = loadComponent(entry.path, entry.status == STATUS.RUNNING, entry.uuid);
            } catch (IOException e) {
                System.out.println("Unable to access jar file " + entry.path);
                return;
            } catch (Exception e) {
                System.out.println("Unable to load jar file " + entry.path);
                return;
            }
            System.out.println("Successfully recovered " + components.get(compId).toString());
        }
        System.out.println("Finished recovering RuntimeEnv from config!");
    }

    public Logger getLogger() {
        return logger;
    }

    public static RuntimeEnv getInstance(){
        if(instance == null){
            instance = new RuntimeEnv();
        }
        return instance;
    }
}
