package Logging;

import java.time.Instant;

public class ConsoleLogger implements Logger{
    @Override
    public void sendLog(String text) {
        System.out.println("++++ LOG: " + text + "([" + Instant.now() + "])");
    }
}
