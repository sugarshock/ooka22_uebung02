package Logging;

import RuntimeEnv.RuntimeEnv;

public class LoggerFactory {
    public static Logger createLogger(){
        return new LoggerProxy(RuntimeEnv.getInstance().getLogger());
    }
}
