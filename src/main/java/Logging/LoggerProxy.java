package Logging;

public class LoggerProxy implements Logger{

    private Logger logger;

    public LoggerProxy(Logger logger){
        this.logger = logger;
    }

    @Override
    public void sendLog(String text) {
        logger.sendLog("Meldung aus Component :"+ text);
    }
}
