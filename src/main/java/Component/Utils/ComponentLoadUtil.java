package Component.Utils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class ComponentLoadUtil {

    public static Class findClassWithAnnotatedMethod(JarInputStream jar, Class annotation, URLClassLoader classLoader) throws ClassNotFoundException, NoSuchMethodException, IOException {
        JarEntry jarEntry;

        while (true) {
            jarEntry = jar.getNextJarEntry();
            if (jarEntry == null) {
                break;
            }
            if(!jarEntry.getName().endsWith(".class")){
                continue;
            }
            // -6 because of .class
            String entryClassName = jarEntry.getName().substring(0,jarEntry.getName().length()-6);
            entryClassName = entryClassName.replace('/', '.');
            Class c = classLoader.loadClass(entryClassName);

            if(firstStaticMethodWithAnnotationOrNull(c, annotation) != null){
                return c;
            }
        }
        throw new ClassNotFoundException("Referenced component JAR does not contain a class with the necessary annotations!");
    }

    public static Method firstStaticMethodWithAnnotationOrNull(Class c, Class annotation) throws NoSuchMethodException {
        List<Method> methods = Arrays.asList(c.getMethods());
        for(Method method : methods){
            if(method.isAnnotationPresent(annotation) && Modifier.isStatic(method.getModifiers())){
                return method;
            }
        }
        return null;
    }
}
