package Component.Utils;

import Annotations.Inject;
import Logging.Logger;
import com.fasterxml.jackson.core.type.TypeReference;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

public class ComponentInjectUtil {
    public static <T> void injectInto(Class targetClass, Class<T> injectedInterface, T injectedInstance) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Field field = firstInjectableMemberOfType(targetClass, injectedInterface);
        if(field == null){
            // Nothing to inject
            return;
        }
        String setterName = field.getName();
        setterName = setterName.substring(0,1).toUpperCase() + setterName.substring(1).toLowerCase();
        setterName = "set" + setterName;
        Method setter = targetClass.getMethod(setterName, injectedInterface);

        if(setter == null){
            throw new NoSuchMethodException("Setter does not exist for injectable field of type " + injectedInterface.getName());
        }
        setter.invoke(injectedInstance);
    }


    public static Field firstInjectableMemberOfType(Class c, Class type){
        Field[] fields = c.getFields();
        for(Field field : fields){
            if(field.isAnnotationPresent(Inject.class) && !Modifier.isStatic(field.getModifiers()) && field.getType() == type){
                return field;
            }
        }
        return null;
    }
}
