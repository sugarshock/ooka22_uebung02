package Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.UUID;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

import Annotations.StartMethod;
import Annotations.StopMethod;
import Component.Utils.ComponentInjectUtil;
import Component.Utils.ComponentLoadUtil;
import Enums.STATUS;
import Logging.Logger;
import RuntimeEnv.RuntimeEnv;

public class Component {

    private UUID id;
    private STATUS status;

    private JarFile jar;

    private Class startClass;

    private Method startMethod;
    private Method stopMethod;

    private File metafile;
    private URL[] urls;
    private URLClassLoader cl;

    public Component(UUID id, String pathToJar) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException {
        this.id = id;
        status = STATUS.STOPPED;
        jar = new JarFile(pathToJar);
        URL[] urls = { new URL("jar:file:" + pathToJar+"!/") };
        URLClassLoader cl = URLClassLoader.newInstance(urls);
        JarInputStream jarFile = new JarInputStream(new FileInputStream(pathToJar));

        startClass = ComponentLoadUtil.findClassWithAnnotatedMethod(jarFile, StartMethod.class, cl);
        startMethod = ComponentLoadUtil.firstStaticMethodWithAnnotationOrNull(startClass, StartMethod.class);
        stopMethod = ComponentLoadUtil.firstStaticMethodWithAnnotationOrNull(startClass, StopMethod.class);
        if(stopMethod == null){
            throw new NoSuchMethodException("The StartClass does not contain a properly annotated StopMethod!");
        }

        try {
            ComponentInjectUtil.injectInto(startClass, Logger.class, RuntimeEnv.getInstance().getLogger());
        } catch (InvocationTargetException e) {
            System.out.println("An error occured while trying to inject logger.");
        }
    }

    public Component(UUID id, Component component) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException {
        this(id, component.jar.getName());
    }

    public void start() throws InvocationTargetException {
        try {
            startMethod.invoke(null);
            status = STATUS.RUNNING;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            throw e;
        }
    }

    public void stop() throws InvocationTargetException {
        try {
            stopMethod.invoke(null);
            status = STATUS.STOPPED;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            throw e;
        }
    }



    public String toString(){
        return "[" + id.toString() + ":" + jar.getName() + ":" + status.name() + "]";
    }

    public UUID getId() {
        return id;
    }

    public STATUS getStatus() {
        return status;
    }

    public String getComponentPath(){
        return jar.getName();
    }

    public Class getStartClassForTestingPurposes(){
        return startClass;
    }
}
